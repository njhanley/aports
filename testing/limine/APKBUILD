# Maintainer: psykose <alice@ayaya.dev>
pkgname=limine
pkgver=3.10.2
pkgrel=0
pkgdesc="Advanced multiprotocol x86/x86_64 BIOS/UEFI Bootloader"
url="https://limine-bootloader.org"
# only x86/x86_64 supported, x86 needs a cross toolchain
arch="x86_64"
license="BSD-2-Clause"
makedepends="
	clang
	mtools
	nasm
	"
subpackages="
	$pkgname-dev
	$pkgname-cd:_cd
	$pkgname-deploy
	$pkgname-pxe
	$pkgname-sys
	$pkgname-32:_32
	$pkgname-64:_64
	"
source="https://github.com/limine-bootloader/limine/releases/download/v$pkgver/limine-$pkgver.tar.xz"
options="!check" # no tests in tarball

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-all
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

deploy() {
	pkgdesc="$pkgdesc (limine-deploy bios installer)"

	amove usr/bin/limine-deploy
}

_cd() {
	pkgdesc="$pkgdesc (cd/efi files)"
	depends="$pkgname-sys"

	amove usr/share/limine/limine-cd*.bin
}

pxe() {
	pkgdesc="$pkgdesc (pxe executable)"
	depends="$pkgname-sys"

	amove usr/share/limine/limine-pxe.bin
}

sys() {
	pkgdesc="$pkgdesc (sys file)"

	amove usr/share/limine/limine.sys
}

_32() {
	pkgdesc="$pkgdesc (32-bit uefi image)"

	amove usr/share/limine/BOOTIA32.EFI
}

_64() {
	pkgdesc="$pkgdesc (64-bit uefi image)"

	amove usr/share/limine/BOOTX64.EFI
}

sha512sums="
cae32d4c5ba8acc61dd3e989f83d83a11d8f7cfdd81c78e5bc9cd8e1a82dd4872b6e7c369d7a3eea409e0cf752252929680f318d27b4c3640d03ad6e1712a6eb  limine-3.10.2.tar.xz
"
