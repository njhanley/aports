# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=riemann-c-client
pkgver=2.0.1
pkgrel=0
pkgdesc="A C client library for the Riemann monitoring system"
url="https://github.com/algernon/riemann-c-client"
arch="all"
license="LGPL-3.0-or-later"
depends_dev="protobuf-c-dev"
makedepends="$depends_dev
	autoconf
	automake
	json-c-dev
	libtool
	wolfssl-dev
	"
checkdepends="check-dev"
subpackages="
	$pkgname-libs
	$pkgname-dev
	$pkgname-doc
	"
source="https://github.com/algernon/riemann-c-client/archive/riemann-c-client-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-riemann-c-client-$pkgver"

prepare() {
	default_prepare

	autoreconf -iv
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-tls
	make
}

check() {
	make check
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="
7c531edea1314afa3a98146c04403f285f7954bb475444475b475a9a3cfa444c1713237847c16b48263d79a80feb0161efb1a2d7aae125370ae0e7434ccdd85d  riemann-c-client-2.0.1.tar.gz
"
